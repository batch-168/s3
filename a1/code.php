<?php


class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printName(){
		return "Your Full Name is $this->firstName $this->middleName $this->lastName";
	}

}

class Developer extends Person{
	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you're a developer";
	}
}

class Engineer extends Person{
	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you're an engineer";
	}
}

$person = new Person('Senku', 'P.', 'Ishigamu');
$developer = new Developer('John', 'Pincgh', 'Smith');
$engineer = new Engineer('Harold', 'Pyers', 'Reeeses');